import React, { Component } from 'react';
import './Letters-view.css';
import * as wordService from '../../services/words.service'
import * as lettersService from '../../services/letters.service'
import LettersContent from './components/letters-content/Letters-content';
import Word from './components/word/Word';

class LettersView extends Component {

  state = {
    letters: [],
    word: [],
    words: [],
    valid: false
  }

  constructor(props) {
    super(props)

    this.selectLetter = this.selectLetter.bind(this);
  }

  componentDidMount() {
    lettersService.getBoard()
      .then(data => {
        this.setState({ letters: [...data.board] });
      })
      .catch(console.log);

    wordService.getWords()
      .then(data => {
        this.setState({ words: [...data.words] });
      })
      .catch(console.log);
  }

  selectLetter(newLetter) {
    if (!newLetter) {
      this.setState({ word: [...[]], valid: false });
      return;
    }
    let value = this.state.word.find(letter => letter.key === newLetter.key)
    if (!value) {
      this.setState({ word: [...this.state.word, newLetter] }, () => { this.validate() })
    } else {
      this.setState({ word: [...this.state.word.filter(letter => letter.key !== value.key)] }, () => { this.validate() })
    }
  }

  validate() {
    let val = this.state.word.map(val => val.letter).toString().split(',').join('');

    this.setState({
      valid: this.state.words.find(word =>
        !word.toUpperCase().localeCompare(val.toUpperCase())
      ) ? true : false
    })   
  }

  clear() {
    this.selectLetter(null);
    this.setState()
  }

  render() {    
    return (
      <div className="letters-view">
        <LettersContent selectLetter={this.selectLetter} {...this.state}></LettersContent>
        <div className="letters-view-div ">
          <h1>Clear Word <button className="button-x" disabled={!this.state.word.length} onClick={() => { this.clear() }}>x</button></h1>
          <Word valid={this.state.valid} word={this.state.word} />
        </div>
      </div>
    );
  }
}

export default LettersView;
