import React, { Component } from 'react';
import './Letters-content.css';
import Letter from '../letter/Letter';

class LettersContent extends Component {

  render() {
    let { letters } = this.props;
    return (
      <div>
        <section className="letters-content-section">
          {letters.map((letter, i) => {
            let select = this.props.word.find(val => val.key === i) ? true : false;
            return (
              <Letter letter={letter} select={select} {...this.props} key={i} i={i} ></Letter>
            )
          })}
        </section>
      </div>
    );
  }
}

export default LettersContent;
