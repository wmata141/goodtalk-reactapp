import React, { PureComponent } from 'react';
import './Letter.css';

class Letter extends PureComponent {
  render() {
    const { letter, i, select, selectLetter } = this.props;
    return (
      <div onClick={() => { selectLetter({ letter: letter, key: i }) }} className={'letter ' + (select ? 'letter-activate' : '')}>
        {letter}
      </div>
    );
  }
}

export default Letter;
