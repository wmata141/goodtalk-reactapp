const data = {
    words: ["array", "arrays", "art", "arts", "fast", "fat", "fist", "lift", "lifts", "lire", "list", "load", "loaf", "loft", "lost", "lure", "lust", "rant", "rat", "rats", "rent", "rest", "rust", "sat", "soft", "sort", "soy", "start", "starts", "street", "tar", "tart", "tarts", "toy", "toys", "tray", "trays"]
}

function getWords() {
    return new Promise((resolve, reject) => {
        setTimeout(_ => {
            resolve(data);
        }, 800)
    });
}

export { getWords }