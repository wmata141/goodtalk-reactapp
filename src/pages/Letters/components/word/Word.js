import React, { PureComponent } from 'react';
import './word.css';

class Word extends PureComponent {
  constructor({ word, valid }) {
    super();
    this.state = {
      word,
      valid,
    }
  }

  componentDidUpdate() {
    this.setState({
      word: this.props.word,
      valid: this.props.valid
    });
  }

  render() {
    return (
      <div className={'word ' + (this.state.valid ? 'word-valid' : '')}>
        <label className="label">
          <div>
            {this.state.word.map((letter, i) => (
              <span
                className="letter-word"
                key={i}>
                {letter.letter}

              </span>
            ))}
          </div>

          <h1>{this.state.valid ? 'valid' : 'invalid'}</h1>
        </label>
      </div>
    );
  }
}

export default Word;
