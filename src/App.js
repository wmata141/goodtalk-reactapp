import React, {Component} from 'react';
import './App.css';
import LettersView from './pages/Letters/Letters-view';

class App extends Component {
  render() {
    return (
      <div name="App">
        <LettersView/>
      </div>
    );
  }
}

export default App;
