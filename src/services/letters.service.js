const data = {
    "board": [
      "L",
      "I",
      "S",
      "T",
      "O",
      "F",
      "A",
      "T",
      "S",
      "T",
      "R",
      "S",
      "O",
      "R",
      "A",
      "Y"
    ]
  }
  
function getBoard() {
    return new Promise((resolve, reject) => {
        setTimeout(_ => {
            resolve(data);
        }, 800)
    });
}

export { getBoard }